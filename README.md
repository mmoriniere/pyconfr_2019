# Après Jupyter notebook, voici JupyterLab

Présentation pour la PyCon Fr 2019 à Bordeaux le 2 novembre.

## Description de ma proposition
Le notebook de Jupyter est un grand succès, si bien que certains y voient le nouvel Excel.
La version 1.0 de son successeur, JupyterLab, est sortie cet été.
Après un rapide coup d'oeil des nouveautés visuelles et fonctionnelles,
je présenterai quelques cas d'utilisation que j'ai apprécié,
spécifiques à JupyterLab ou non (édition LateX, visualisation de cartes,
utilisation de nbformat pour créer des notebooks de manière programmatique,
conversion en pages de documentation avec nbsphinx, ...). 
Je donnerai enfin quelques références utiles et parlerai d'autres projets reliés
à l'écosystème jupyter que je n'ai pas encore testé mais qui me semblent intéressants à suivre.

## Utilisation de ce projet

### Mise en place

```bash
git clone git@gitlab.com:mmoriniere/pyconfr_2019.git
cd pyconfr_2019/
git clone https://github.com/hakimel/reveal.js.git
cd reveal.js && git checkout 3.5.0 && cd ..  # utiliser la version supportée par jupyter de reveal.js
python3.X -m virtualenv .venv  # Remplacer X par 6 ou 7 (pas testé pour 8)
source .venv/bin/activate
pip install -r requirements.txt
```

### Utilisation

Pour modifier le notebook, qui sert de référence pour créer les diapositives, lancer jupyter lab :
```bash
jupyter lab
```

Pour exporter le notebook sous forme de slides et les servir :
```
jupyter nbconvert --to slides pyconfr_2019.ipynb --reveal-prefix reveal.js --SlidesExporter.reveal_theme=league --post serve
```
Cette commande permet de convertir le notebook en slides, d'utiliser la version locale de reveal.js et d'utiliser le thème voulu pour les slides.

## Références utiles

### Towards data science / Medium
- https://towardsdatascience.com/jupyter-is-the-new-excel-but-not-for-your-boss-d24340ebf314
- https://towardsdatascience.com/jupyter-is-the-new-excel-a7a22f2fc13a
- https://towardsdatascience.com/jupyter-notebook-extensions-517fa69d2231
- https://medium.com/@brianray_7981/jupyterlab-first-impressions-e6d70d8a175d
- https://towardsdatascience.com/jupyter-lab-evolution-of-the-jupyter-notebook-5297cacde6b

### Jupyter notebook
- https://github.com/jupyter/notebook
- https://jupyter-notebook.readthedocs.io/en/stable/

### JupyterLab
- https://jupyterlab.readthedocs.io/en/stable/user/interface.html
- https://github.com/jupyterlab/jupyterlab

### jupyterlab-latex
- https://github.com/jupyterlab/jupyterlab-latex

### ipyleaflet
- https://github.com/jupyter-widgets/ipyleaflet
- https://ipyleaflet.readthedocs.io/en/latest/

### nbformat
- https://nbformat.readthedocs.io/en/latest/api.html#module-nbformat.v4

### slideshow
- https://docs.microsoft.com/fr-fr/azure/notebooks/present-jupyter-notebooks-slideshow
- https://medium.com/learning-machine-learning/present-your-data-science-projects-with-jupyter-slides-75f20735eb0f
- https://medium.com/@mjspeck/presenting-code-using-jupyter-notebook-slides-a8a3c3b59d67

### JupyterHub
- https://jupyter.org/hub
- https://jupyterhub.readthedocs.io/en/stable/
- https://github.com/jupyterhub/jupyterhub