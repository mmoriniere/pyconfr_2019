# Qui je suis ?

Mon parcours (ingénieur, docteur). 
Pourquoi je présente ce sujet :
- utilisateur de Jupyter notebook quasiment depuis que je fais du python,
- passé à Jupyter Lab dès la sortie de la beta, je ne suis plus revenu à notebook.
- Je n'ai jamais participé au développement des outils dont je vais parler,
  mais j'ai aimé les utiliser.
- Cette présentation sera donc un retour d'expérience. N'attendez pass de moi
  une présentation globale, mais plutôt des choses que j'ai testées et approuvées.

Premier talk à propos de python, merci aux organisateurs de m'avoir accepté.

# OctopusMind

Fondée il y a 15 ans, basé à Nantes.
Ce qu'on fait (exploration de données ouvertes, civic tech), pourquoi on est là
(Open source, python-django + machine learning)

# Questions préliminaires

- Qui connaît Jupter Notebook ? Qui a déjà utilisé JN ? Qui l'utilise au quotidien ?
- Qui connaît Jupter Lab ? Qui a déjà utilisé JL ? Qui l'utilise au quotidien ?

# Un peu d'histoire

ipython, jupyter, notebook, hub, lab (me renseigner sur tout ça). 
Le format du notebook. 
A quoi ressemble JN ("Nouveau Excel").
Plusieurs langages de programmation accessibles (kernels).

# JupyterLab, pour quoi faire ?

## Nouvelle interface, nouvelles fonctionnalités

Motivation de l'équipe jupyter (si je trouve)

- Possibilité de découper en sous-fenêtres (et de se faire un dashboard sympa),
- Panneau latéral (plusieurs onglets utiles, à détailler :
  - arborescence des fichiers,
  - gestion plugins/extensions,
  - gestion des notebooks utilisés,
  - ...
- Menu complet en haut (en particulier : changement de couleur de thème, ...)
- Déplacer les cellules à la souris,
- Sélection de cellules pour les exécuter,
- Possibilité de faire des aperçus,
- Ça ressemble de plus en plus à un éditeur complet (même s'il manque encore des choses)

## Nouvelle façon d'intégrer des extensions

Je m'y connais moins, mais je vais essayer d'expliquer les différences entre les deux.

Il se peut qu'il y ait des plugins de JN qui ne sont pas encore portés vers JL 
(essayer de voir s'ils sont nombreux ou pas)

# Mon utilisation de JL (et JN)

Ce que je vais présenter ici, ce sont des choses que j'ai faites avec JL.
Ayant délaissé JN, je ne suis pas certain que tout y soit possible,
mais ça doit être le cas de la majorité des choses que je vais présenter ici.

## Edition LateX (ou comment remplacer Overleaf)

Qui utilise LateX ? Qui connaît Overleaf ?
Il suffit d'installer l'extension jupyterlab-latex,
on peut ensuite prévisualiser nos modifications à chaque sauvegarde du fichier.

Manque juste un correcteur orthographique et une meilleure autocomplétion
pour que ça soit vraiment pratique (ça ferait de jupyterlab un super éditeur).

## Documentation avec nbsphinx

Permet de se servir de notebooks en tant que fichier. Installation, utilisation.
Je m'en suis servi de base pour de la doc fonctionnelle (comment se servir d'un package),
et aussi pour préparer des tutoriels "à trou".
Pour avoir été des deux côtés, c'est plus intéressant qu'un tuto où
il faut juste lire et rentrer les commandes qui nous sont demandées.
Conversion de notebooks en différents formats (nbconvert).

On peut aussi faire des présentations avec des notebooks.
http://damianavila.github.io/scipy2013_talks/index.html#/
`jupyter nbconvert --to slides Untitled.ipynb --reveal-prefix reveal.js --SlidesExporter.reveal_theme=league --post serve`
Pour avoir un slide qui tourne en mode orateur,
besoin de faire `git clone reveal.js; touch custom.css reveal.js/***/head.min.js`
(pour voir vos notes pendant la présentation)

## Créer des notebooks de manière programmatique

Il peut arriver qu'on veuille créer des notebooks de manière programmatique. 
Cas d'utilisation : faire des expériences numériques,
pour tester le même notebook mais avec toute une série de paramètres.
Détails : 
- faire tourner un code de simulation numérique (coûteux en temps et en resources). 
- Je me suis fait une petite bibliothèque pour gérer les entrées et sorties (yaml la plupart du temps)
- Plusieurs paramètres à tester, je commençais à toujours faire plus ou moins le même notebook, que je copiais-collais, je changeais les paramètres, ...
- C'est pas super fiable, et vite casse-pied (surtout si on change un bout de code, la façon de faire un graphique) : et si je faisais ça de manière programmatique ?
- Réponse : nbformat ! Installation et utilisation (initialisation de notebook, ajout de cellules (markdown ou code)).
- Ça m'a permis de facilement créer tous les notebooks que je voulais, les lancer, récupérer les données des simulations, créer les visualisations.
- Si je voulais changer le code du post-processing, j'avais juste à le changer à un endroit, relancer les notebooks, et ça me refaisait mes visualisations.

Autres utilisations chez Netflix :
https://medium.com/netflix-techblog/notebook-innovation-591ee3221233

## Visualisation de cartes avec ipyleaflet

Installation.
Utilisation de gpxpy (bof, bof...) pour afficher un trajet et des points sur la carte.

## Visualisation 3D avec ipyvolume

Installation.
Utilisation.

# Autres projets jupyter à suivre

- JupyterHub
  "The Hub can offer notebook servers to a class of students,
  a corporate data science workgroup, a scientific research project,
  or a high performance computing group."
- Voilà
- bqplot

# Références utiles

